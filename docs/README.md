![Logo](../docs/images/logo.png)

This file is in the `docs` directory.

Any **document or help files** which are part of the project should be kept **in this directory**.
This typically includes users guides, specs, dev guides, image files.

---
NOTE: 

This file is a placeholder, used to preserve directory structure in Git.

This file does not need to be edited.