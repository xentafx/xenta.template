![Logo](../docs/images/logo.png)

This file is in the `src` directory.

Any **base framework projects** which are part of the project should be kept **in this directory**.
This typically includes base business logic, framework libraries.

---
NOTE: 

This file is a placeholder, used to preserve directory structure in Git.

This file does not need to be edited.