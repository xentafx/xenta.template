![Logo](../docs/images/logo.png)

This file is in the `config` directory.

Any **application configuration files** which are part of the project should be kept **in this directory**.
This typically includes various development, production and deployment files.

---
NOTE: 

This file is a placeholder, used to preserve directory structure in Git.

This file does not need to be edited.