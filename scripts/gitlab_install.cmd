
@ECHO OFF

:: Output dotnet info

dotnet --info

SET CI_BUILD=%GITLAB_CI%

SET number=00000%CI_JOB_ID%

SET XENTA_VERSION=0.0.1

SET XENTA_VERSION_SUFFIX=%CI_COMMIT_REF_NAME%-%number:~-5%

IF NOT "%CI_COMMIT_TAG%"=="" SET XENTA_VERSION_SUFFIX=%CI_COMMIT_TAG:~6,5%

IF NOT "%XENTA_VERSION_SUFFIX%"=="" (

    SET XENTA_VERSION_SUFFIX=%XENTA_VERSION_SUFFIX: =%

    SET XENTA_DASH_VERSION_SUFFIX=-%XENTA_VERSION_SUFFIX%

) ELSE (

    SET XENTA_DASH_VERSION_SUFFIX=

)   

echo "Version Suffix:" %XENTA_VERSION_SUFFIX%

SET BUILD_TYPE=Release

IF "%CI_COMMIT_REF_NAME%"=="master" COPY config\nuget-master.config .\nuget.config

IF "%CI_COMMIT_REF_NAME%"=="dmz" COPY config\nuget-dmz.config .\nuget.config

IF NOT "%CI_COMMIT_TAG%"=="" COPY config\nuget.config .\nuget.config

IF "%CI_COMMIT_REF_NAME%"=="master" COPY config\versions-master.props .\versions.props

IF "%CI_COMMIT_REF_NAME%"=="dmz" COPY config\versions-dmz.props .\versions.props

IF NOT "%CI_COMMIT_TAG%"=="" COPY config\versions.props .\versions.props

IF "%CI_COMMIT_REF_NAME%"=="dmz" SET BUILD_TYPE=Debug