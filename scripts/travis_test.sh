
#!/bin/bash

# Run unit tests

cd tests/Example.Tests

dotnet restore --configfile ../../nuget.config

dotnet test

if [[ $? != 0 ]]; then exit 1 ; fi

cd ../..