
#!/bin/bash   

dotnet --info

export CI_BUILD=True

export XENTA_VERSION=0.0.1

if [[ "$TRAVIS_BRANCH" == "master" ]]; then cp config/nuget-master.config ./nuget.config ; fi

if [[ "$TRAVIS_BRANCH" == "dmz" ]]; then cp config/nuget-dmz.config ./nuget.config ; fi

if [[ "$TRAVIS_TAG" != "" ]]; then cp config/nuget.config ./nuget.config ; fi

if [[ "$TRAVIS_BRANCH" == "master" ]]; then cp config/versions-master.props ./versions.props ; fi

if [[ "$TRAVIS_BRANCH" == "dmz" ]]; then cp config/versions-dmz.props ./versions.props ; fi

if [[ "$TRAVIS_TAG" != "" ]]; then cp config/versions.props ./versions.props ; fi