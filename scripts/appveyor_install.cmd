
@ECHO OFF

:: Output dotnet info

dotnet --info

SET CI_BUILD=%APPVEYOR%

SET number=00000%APPVEYOR_BUILD_NUMBER%

SET XENTA_VERSION=0.0.1

SET XENTA_VERSION_SUFFIX=%APPVEYOR_REPO_BRANCH%-%number:~-5%

IF NOT "%APPVEYOR_REPO_TAG_NAME%"=="" SET XENTA_VERSION_SUFFIX=%APPVEYOR_REPO_TAG_NAME:~6,5%

IF NOT "%XENTA_VERSION_SUFFIX%"=="" (

    SET XENTA_VERSION_SUFFIX=%XENTA_VERSION_SUFFIX: =%

    SET XENTA_DASH_VERSION_SUFFIX=-%XENTA_VERSION_SUFFIX%

) ELSE (

    SET XENTA_DASH_VERSION_SUFFIX=

)   

echo "Version Suffix:" %XENTA_VERSION_SUFFIX%

SET BUILD_TYPE=Release

IF "%APPVEYOR_REPO_BRANCH%"=="master" COPY config\nuget-master.config .\nuget.config

IF "%APPVEYOR_REPO_BRANCH%"=="dmz" COPY config\nuget-dmz.config .\nuget.config

IF NOT "%APPVEYOR_REPO_TAG_NAME%"=="" COPY config\nuget.config .\nuget.config

IF "%APPVEYOR_REPO_BRANCH%"=="master" COPY config\versions-master.props .\versions.props

IF "%APPVEYOR_REPO_BRANCH%"=="dmz" COPY config\versions-dmz.props .\versions.props

IF NOT "%APPVEYOR_REPO_TAG_NAME%"=="" COPY config\versions.props .\versions.props

IF "%APPVEYOR_REPO_BRANCH%"=="dmz" SET BUILD_TYPE=Debug