:: @ECHO OFF

:: Build packages

cd src\Example

dotnet restore --configfile ..\..\nuget.config

IF NOT "%APPVEYOR_REPO_TAG_NAME%"=="" (

    IF NOT "%XENTA_VERSION_SUFFIX%"=="" (

        dotnet pack --configuration %BUILD_TYPE% --version-suffix %XENTA_VERSION_SUFFIX%

    ) ELSE (

        dotnet pack --configuration %BUILD_TYPE%

    )    

)

IF "%APPVEYOR_REPO_TAG_NAME%"=="" (dotnet pack --configuration %BUILD_TYPE% --version-suffix %XENTA_VERSION_SUFFIX% --include-symbols --include-source)

cd ..\..