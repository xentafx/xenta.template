:: @ECHO OFF

set DOTNET_CLI_TELEMETRY_OPTOUT=1

:: Build packages

cd src\Example

dotnet restore --configfile ..\..\nuget.config

:: IF NOT "%CI_COMMIT_TAG%"=="" (

::    IF NOT "%XENTA_VERSION_SUFFIX%"=="" (

::        dotnet pack --configuration %BUILD_TYPE% --version-suffix %XENTA_VERSION_SUFFIX%

::    ) ELSE (

::        dotnet pack --configuration %BUILD_TYPE%

::    )    

::)

:: IF "%CI_COMMIT_TAG%"=="" (dotnet pack --configuration %BUILD_TYPE% --version-suffix %XENTA_VERSION_SUFFIX% --include-symbols --include-source)

cd ..\..