![Logo](../docs/images/logo.png)

This file is in the `scripts` directory.

Any **executable scripts** which are part of the project should be kept **in this directory**.
This typically includes build scripts, targets, bat and bash files.

---
NOTE: 

This file is a placeholder, used to preserve directory structure in Git.

This file does not need to be edited.