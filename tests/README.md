![Logo](../docs/images/logo.png)

This file is in the `tests` directory.

Any **unit test** which are part of the project should be kept **in this directory**.
This typically **Any** xunit, msunit, nunit projects.

---
NOTE: 

This file is a placeholder, used to preserve directory structure in Git.

This file does not need to be edited.