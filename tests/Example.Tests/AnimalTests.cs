using Example.Animals;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Example.Tests
{
    [TestClass]
    public class AnimalTests
    {
        [TestMethod]
        public void The_Cat_Should_Meow()
        {
            // Given
            var cat = new Cat();

            // When
            var results = cat.Talk();
            
            // Then
            Assert.AreEqual("Meow", results);
        }

        [TestMethod]
        public void The_Dog_Should_Bark()
        {
            // Given
            var dog = new Dog();

            // When
            var results = dog.Talk();

            // Then
            Assert.AreEqual("Woof", results);


        }
    }
}
