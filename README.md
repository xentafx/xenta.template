![Logo](docs/images/logo.png)

[![Badge](https://img.shields.io/badge/xentafx-oss-green.svg)](https://gitlab.com/xentafx/xenta.template) [![License](https://img.shields.io/badge/license-Apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0) [![Target](https://img.shields.io/badge/targets-.netfx%20%7C%20.netstandard%20%7C%20.netcore%20%7C%20mono%20%7C%20xamarin-orange.svg)](https://gitlab.com/xentafx/xenta.template)

Xenta.Template is a generic project template used as a base for all xenta projects. 

## Continuous integration

| Build server                | Platform     | Build Status Branch - Master                                                                                                                                        | Build Status Branch - DMZ                                                                                                                                           |
|-----------------------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AppVeyor                    | Windows      | [![Build Status](https://ci.appveyor.com/api/projects/status/7a33242t19k5dv9b/branch/master?svg=true)](https://ci.appveyor.com/project/mrpastewart/xenta-template/branch/master)   | [![Build Status](https://ci.appveyor.com/api/projects/status/7a33242t19k5dv9b/branch/dmz?svg=true)](https://ci.appveyor.com/project/mrpastewart/xenta-template/branch/dmz)      |
| Travis                      | Linux / OS X | [![Build Status](https://travis-ci.org/xentafx/xenta.template.svg?branch=master)](https://travis-ci.org/xentafx/xenta.template)                                     | [![Build Status](https://travis-ci.org/xentafx/xenta.template.svg?branch=dmz)](https://travis-ci.org/xentafx/xenta.template)                                        |
| GitLab                      | Windows      | [![Build Status](https://gitlab.com/xentafx/xenta.template/badges/master/pipeline.svg)](https://gitlab.com/xentafx/xenta.template/commits/master)                   | [![Build Status](https://gitlab.com/xentafx/xenta.template/badges/dmz/pipeline.svg)](https://gitlab.com/xentafx/xenta.template/commits/dmz) &nbsp;                  |

### Preview of any media used in the project i.e screenshots, video
![Alt text](http://www.addictedtoibiza.com/wp-content/uploads/2012/12/example.png)

## Table of Contents

- [Introduction](#introduction)
    - [Project Goals](#project-goals)
    - [OSS Projects Used](#oss-projects-used)
    - [Project Structure](#project-structure)
- [Installation](#installation)
    - [Requirements](#requirements)
    - [Deployment](#deployment)
    - [Developers](#developers)
- [Developers Setup](#development-setup)
    - [Base Setup](#base-setup)
    - [Addon Setup](#addon-setup)
    - [Extension Packs](#extension-packs)
    - [Extending Psito](#extending-psito)
- [Configurations](#configurations)
    - [IDE Packages](#ide-packages)
    - [Scripts](#scripts)
    - [Config Files](#config-files)
    - [Project Sources](#project-sources)
- [Contributions](#contributions)
- [License](#license)
- [Links](#links)

## Introduction

Give a introduction of the project.

### Project Goals

State the goals and mission of the project

### OSS Projects Used

The {NAME} Project attempts to combine the best in class open source projects that provide meaningful usage within our project scope. The following OSS projects have been chosen for inclusion.

**This list is a moving target and may be updated as the project moves forward to add or delete projects as the needs become clearer.**

### Project Structure

Downloading the source distribution from Github creates the following file structure;
````
{root}
  +-- config
  +-- docs
  +-- logs
  +-- media
  +-- repo
  +-- scripts
  +-- source
  +-- template
  |    +-- App
  |         +-- AppInfo  
  |         +-- <bash.bat>
  +-- tools
  |    +-- Launchers
  +-- <.editorconfig>>
  +-- <.gitattributes>
  +-- <.gitignore>
  +-- <makefile>
  +-- <readme.md>
````

Downloading the release distribution from Github creates the following file structure;
````
{root}
  +-- bin
  +-- docs
  +-- host
  +-- lang
  +-- lib
  |    +-- apache
  |         +-- conf
  |         +-- <>
  +-- scripts
  +-- webapps
  +-- webroot
  |    +-- <>
  +-- <atom.exe>
  +-- <bash.exe>
  +-- <psitomanager.exe>
  +-- <readme.html>
  +-- <readme.md>
````

## Installation

### Requirements

### Deployment

### Developers

## Developers Setup

### Base Setup

### Addon Setup

### Extension Packs

### Extending Psito

## Configurations

### IDE Packages

### Scripts

### Config Files

### Project Sources

## Contributions

## License

Copyright 2000 - 2017 Xenta SocialLabs

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
limitations under the License.

## Links
